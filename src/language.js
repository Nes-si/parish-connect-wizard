export default {
  methods: {
    valueForKey (key) {
      return this.$store.state.nav.languageKeys[key];
    }
  }
}
