import 'babel-polyfill';

import Vue from 'vue';
import VeeValidate from 'vee-validate';

import store from 'store/index';
import App from 'components/app';
import language from 'language';


Vue.use(VeeValidate);

Vue.config.devtools = true;

const dictionary = {
  en: {
    messages: {
      required: () => `This field is required.`
    }
  }
};

//console.log(VeeValidate);
VeeValidate.Validator.updateDictionary(dictionary);

Vue.mixin({
  mixins: [language]
});

store.dispatch('initializeStart');

new Vue({
  el: '#app',
  store,
  render: h => h(App)
});
