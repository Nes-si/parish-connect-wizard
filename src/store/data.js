import {Parse} from 'parse';

import parishJSON from 'parish.json';




//==== without Parish account relation
const ParishConnectLanguage = Parse.Object.extend('ParishConnectLanguage');
const Title = Parse.Object.extend('Title');
const Position = Parse.Object.extend('Position');
const MassType = Parse.Object.extend('MassType');
const OtherTimeType = Parse.Object.extend('OtherTimeType');
const MediaItem = Parse.Object.extend('MediaItem');

//========= Parish account
const ParishAccount = Parse.Object.extend('ParishAccount');

//=========== with Parish account relation
const Parish = Parse.Object.extend('Parish');
const About = Parse.Object.extend('About');
const MissionStatement = Parse.Object.extend('MissionStatement');
const ParishAdministration = Parse.Object.extend('ParishAdministration');
const ParishAdminBio = Parse.Object.extend('ParishAdminBio');
const School = Parse.Object.extend('School');
const Rectory = Parse.Object.extend('Rectory');

//many
const SliderMedia = Parse.Object.extend('SliderMedia');
const MassTime = Parse.Object.extend('MassTime');
const ConfessionTime = Parse.Object.extend('ConfessionTime');
const OtherTime = Parse.Object.extend('OtherTime');
const Sacrament = Parse.Object.extend('Sacrament');
const Ministry = Parse.Object.extend('Ministry');
const OtherProgram = Parse.Object.extend('OtherProgram');
const Staff = Parse.Object.extend('Staff');
const SupportedLanguage = Parse.Object.extend('SupportedLanguage');
const Bulletin = Parse.Object.extend('Bulletin');
const Event_type = Parse.Object.extend('Event');
const Announcement = Parse.Object.extend('Announcement');

const ModelsCommon = [ParishConnectLanguage, Title, Position, MassType, OtherTimeType, MediaItem, Parish];
const ModelsSingle = [Parish, About, MissionStatement, ParishAdministration, ParishAdminBio, School, Rectory];
const ModelsMulti = [SliderMedia, MassTime, ConfessionTime, OtherTime, Sacrament, Ministry, OtherProgram, Staff, SupportedLanguage, Bulletin, Event_type, Announcement];


const firstCharCase = (str, upper = false) => {
  let first = str[0].toLowerCase();
  if (upper)
    first = str[0].toUpperCase();
  return first + str.slice(1);
};

const promisify = obj => {
  return new Promise((resolve, reject) => {
    obj.then(resolve, reject);
  });
};

const date2str = date =>
  date.toLocaleString('ru', {hour: 'numeric', minute: 'numeric'});

const str2date = str => {
  let timeArray = str.split(':');
  if (timeArray.length < 2)
    return new Date(1970, 1, 1, 11, 0);
  return new Date(1970, 1, 1, timeArray[0], timeArray[1]);
};

const days2array = p_obj => {
  let res = [];
  if (p_obj.get('monday'))    res.push(1);
  if (p_obj.get('tuesday'))   res.push(2);
  if (p_obj.get('wednesday')) res.push(3);
  if (p_obj.get('thursday'))  res.push(4);
  if (p_obj.get('friday'))    res.push(5);
  if (p_obj.get('saturday'))  res.push(6);
  if (p_obj.get('sunday'))    res.push(7);
  return res;
};

const array2days = (days, p_obj) => {
  p_obj.set('monday',   days.indexOf(1) != -1);
  p_obj.set('tuesday',  days.indexOf(2) != -1);
  p_obj.set('wednesday',days.indexOf(3) != -1);
  p_obj.set('thursday', days.indexOf(4) != -1);
  p_obj.set('friday',   days.indexOf(5) != -1);
  p_obj.set('saturday', days.indexOf(6) != -1);
  p_obj.set('sunday',   days.indexOf(7) != -1);
};

const filterSpecials = str => {
  if (!str)
    return '';
  if (str.match(/^\d/))
    str = '_' + str;
  return str.replace(/\W/g, "_");
};


export default {
  state: {
    parish: parishJSON,
    
    common: {
      parishConnectLanguage: [],
      title: [],
      position: [],
      massType: [],
      otherTimeType: [],
      mediaItem: []
    },
    
    parse: {
      common: {
        parishConnectLanguage: [],
        title: [],
        position: [],
        massType: [],
        otherTimeType: [],
        mediaItem: [],
        parish: []
      },
  
      parishAccount: null,
      
      single: {
        parish: null,
        about: null,
        missionStatement: null,
        parishAdministration: null,
        parishAdminBio: null,
        rectory: null,
        school: null
      },
      
      multi: {
        sliderMedia: [],
        massTime: [],
        confessionTime: [],
        otherTime: [],
        sacrament: [],
        ministry: [],
        otherProgram: [],
        staff: [],
        bulletin: [],
        event: [],
        announcement: []
      }
    }
  },
  
  mutations: {
    onGetAllData(state) {
      let p_parish = state.parse.single.parish;
      
      for (let key in state.parish) {
        if (key == 'domain') {
          let domain = state.parse.parishAccount.get('domainURL');
          if (domain)
            state.parish.domain = domain;
          continue;
        }
        if (key == 'themeColor') {
          let color = state.parse.parishAccount.get('colorMain');
          if (color)
            state.parish.themeColor = color;
          continue;
        }
        if (key == 'imageAbout' || key == 'imageLogo' || key == 'imageMission') {
          let mediaItem = p_parish.get(key);
          if (mediaItem && mediaItem.get('file'))
            state.parish[key] = mediaItem;
          continue;
        }
        
        let val = state.parish[key];

        if (typeof val !== 'object') {
          let p_val = p_parish.get(key);
          if (typeof p_val !== 'object' || p_val instanceof Date || !p_val) {
            state.parish[key] = p_val;
          } else if (p_val.className == 'MediaItem') {
            state.parish[key] = p_val.get('file').url();
          }
        }
        
        let safeAsg = (key1, value, key2) => {
          if (value)
            val[key1] = key2 ? value.get(key2) : value;
        };
        
        switch (key) {
          case 'about':
            safeAsg('detail', state.parse.single.about.get('detail'));
            break;

          case 'missionStatement':
            safeAsg('detail', state.parse.single.missionStatement.get('detail'));
            break;

          case 'parishAdminBio':
            safeAsg('bio', state.parse.single.parishAdminBio.get('bio'));
            break;
          
          case 'parishAdministration':
            let p_admin = state.parse.single.parishAdministration;
            safeAsg('nameFirst',p_admin.get('nameFirst'));
            safeAsg('nameLast', p_admin.get('nameLast'));
            safeAsg('title',    p_admin.get('title'), 'name');
            safeAsg('position', p_admin.get('position'), 'name');
            
            let mediaItem = p_admin.get('image');
            if (mediaItem && mediaItem.get('file'))
              val.image = mediaItem;
            
            break;

          case 'rectory':
            let p_rectory = state.parse.single.rectory;
            safeAsg('nameFirst',p_rectory.get('nameFirst'));
            safeAsg('nameLast', p_rectory.get('nameLast'));
            safeAsg('street',   p_rectory.get('street'));
            safeAsg('city',     p_rectory.get('city'));
            safeAsg('state',    p_rectory.get('state'));
            safeAsg('zip',      p_rectory.get('zip'));
            safeAsg('phone',    p_rectory.get('phone'));
            safeAsg('email',    p_rectory.get('email'));
            safeAsg('title',    p_rectory.get('title'), 'name');
  
            break;

          case 'staff':
            state.parish[key] = [];
            for (let p_staff of state.parse.multi.staff) {
              let obj = {
                nameFirst:p_staff.get('nameFirst'),
                nameLast: p_staff.get('nameLast'),
                phone:    p_staff.get('phone'),
                email:    p_staff.get('email')
              };
              let pos = p_staff.get('position');
              obj.position = pos ? pos.get('name') : '';
                
              state.parish[key].push(obj);
            }
            break;
            
          case 'massTime':
            state.parish[key] = [];
            for (let p_mass of state.parse.multi.massTime) {
              state.parish[key].push({
                type: p_mass.get('massType').get('name'),
                timeStart: date2str(p_mass.get('timeStart')),
                timeEnd: date2str(p_mass.get('timeEnd')),
                days: days2array(p_mass),
                language: p_mass.get('language') ? p_mass.get('language').get('encodedValue') : 'en'
              });
            }
            break;

          case 'confessionTime':
            state.parish[key] = [];
            for (let p_conf of state.parse.multi.confessionTime) {
              state.parish[key].push({
                timeStart: date2str(p_conf.get('timeStart')),
                timeEnd: date2str(p_conf.get('timeEnd')),
                days: days2array(p_conf),
                language: p_conf.get('language') ? p_conf.get('language').get('encodedValue') : 'en'
              });
            }
            break;

          case 'otherTime':
            state.parish[key] = [];
            for (let p_other of state.parse.multi.otherTime) {
              state.parish[key].push({
                type: p_other.get('type').get('name'),
                timeStart: date2str(p_other.get('timeStart')),
                timeEnd: date2str(p_other.get('timeEnd')),
                days: days2array(p_other),
                language: p_other.get('language') ? p_other.get('language').get('encodedValue') : 'en'
              });
            }
            break;

          case 'sacrament':
            state.parish[key] = [];
            for (let p_sacr of state.parse.multi.sacrament) {
              state.parish[key].push({
                name: p_sacr.get('name'),
                detail: p_sacr.get('detail')
              });
            }
            break;

          case 'ministry':
            state.parish[key] = [];
            for (let p_ministry of state.parse.multi.ministry) {
              state.parish[key].push({
                name: p_ministry.get('name'),
                detail: p_ministry.get('detail')
              });
            }
            break;

          case 'otherProgram':
            state.parish[key] = [];
            for (let p_other of state.parse.multi.otherProgram) {
              state.parish[key].push({
                name: p_other.get('name'),
                detail: p_other.get('detail')
              });
            }
            break;
  
          case 'sliderMedia':
            state.parish[key] = [];
            for (let p_sl of state.parse.multi.sliderMedia) {
              let media = p_sl.get('image');
              if (media && media.get('file'))
                state.parish[key].push({
                  position: p_sl.get('position'),
                  image: media
                });
            }
            break;
        }
        
      }
    },
    
    setObjectsCommmon(state, objects) {
      if (!objects.length)
        return;
      let key = firstCharCase(objects[0].className);
      state.parse.common[key] = objects;
      
      if (key == 'parish')
        return;
      for (let p_obj of objects) {
        let obj = {};
        obj.name = p_obj.get('name');
        
        if (key == 'parishConnectLanguage') {
          obj.encodedValue = p_obj.get('encodedValue');
        } else {
          let language = p_obj.get('language');
          if (language)
            obj.language = language.get('encodedValue');
        }
  
        state.common[key].push(obj);
      }
    },
    setObjectSingle(state, object) {
      let key = firstCharCase(object.className);
      state.parse.single[key] = object;
    },
    setObjectsMulti(state, objects) {
      if (!objects.length)
        return;
      let key = firstCharCase(objects[0].className);
      state.parse.multi[key] = objects;
    },
    setParishAccount(state, object) {
      state.parse.parishAccount = object;
    },
  
    updateJson (state, {parentKey, key, value}) {
      if (parentKey) {
        state.parish[parentKey][key] = value;
      
        let p_obj = state.parse.single[parentKey];
        if (!p_obj)
          return;
      
        if (typeof p_obj.get(key) !== 'object' || typeof value === 'object') {
          p_obj.set(key, value);
        } else {
          for (let p_obj2 of state.parse.common[key]) {
            if (p_obj2.get('name') == value)
              p_obj.set(key, p_obj2);
          }
        }
      
      } else {
        state.parish[key] = value;
      
        if (key == 'domain') {
          state.parse.parishAccount.set('domainURL', value);
        } else if (key == 'themeColor') {
          state.parse.parishAccount.set('colorMain', value);
        } else if (key == 'domainIsOwn' || key == 'schoolGrade' || key == 'schoolHigh') {
          
        } else {
          let multi = state.parse.multi[key];
          if (!multi)
            state.parse.single.parish.set(key, value);
        }
      }
    },
  },
  
  actions: {
    initialData ({commit, dispatch, rootState}) {
      let promises = [];
      for (let model of ModelsCommon) {
        promises.push(promisify(
          new Parse.Query(model)
            .find()
        ).then(objects => commit('setObjectsCommmon', objects)));
      }
      
      Promise.all(promises)
        .then(() => {
          commit('incrementStep');
          
          if (rootState.initialize.accountId) {
            new Parse.Query(ParishAccount)
              .get(rootState.initialize.accountId)
              .then(account => {
                if (account)
                  dispatch('getAllData', account);
                else
                  commit('setLoadingState', false);
              }, () => commit('setLoadingState', false));
          } else {
            commit('setLoadingState', false);
          }
        }, e => console.log(e));
    },
    
    setParish ({dispatch, commit}, selectedParish) {
      commit('setLoadingState', true);
      
      // making a new parish
      if (selectedParish == "0") {
        let account = new ParishAccount();
        account.set('name', 'from wizard');
        account.set('owner', Parse.User.current());
        account.save()
          .then(() => {
            dispatch('getAllData', account);
          });
        
      } else {
        let account = selectedParish.get('account');
        account
          .fetch()
          .then(obj => dispatch('getAllData', account));
      }
    },
    
    getAllData ({commit, state}, account) {
      commit('setParishAccount', account);
      
      let promises = [];
      
      for (let model of ModelsSingle) {
        promises.push(promisify(
          new Parse.Query(model)
            .equalTo('account', account)
            .first()
        ).then(object => {
          if (object) {
            commit('setObjectSingle', object);
            return;
          }
          
          object = new model();
          object.set('account', account);
          
          switch (object.className) {
            case 'ParishAdministration':
              object.set('title', state.parse.common.title[0]);
              object.set('position', state.parse.common.position[0]);
              break;
              
            case 'Rectory':
              object.set('title', state.parse.common.title[0]);
              break;
          }
          
          return promisify(object.save())
            .then(() => commit('setObjectSingle', object));
        }));
      }
      
      for (let model of ModelsMulti) {
        promises.push(promisify(
          new Parse.Query(model)
            .equalTo('account', account)
            .find()
        ).then(objects => commit('setObjectsMulti', objects)));
      }
  
      Promise.all(promises)
        .then(() => {
          commit('onGetAllData');
          commit('incrementStep');
          commit('setLoadingState', false);
        }, e => console.log(e));
    },
    
    createMediaItem({commit}, file) {
      commit('setLoadingState', true);
      let name = filterSpecials(file.name);
      if (!name)
        name = 'image';
      let parseFile = new Parse.File(name, file, file.type);
      return promisify(parseFile.save())
        .then(() => {
          let item = new MediaItem();
          item.set('file', parseFile);
          item.set('name', name);
          item.set('type', 'MEDIA_TYPE__IMAGE');
          commit('setLoadingState', false);
          return promisify(item.save());
        })
        .catch(e => console.log(e));
    },
  
    // store current parish info in TempJson table in backend
    updateTempDB ({commit, rootState, state}, keys) {
      commit('setShowingErrors', true);
      
      let valid = true;
      let errors = rootState.nav.currentStepErrors;
      for (let errKey in errors) {
        if (errors[errKey])
          valid = false;
      }
    
      if (valid) {
        commit('setShowingErrors', false);
        commit('incrementStep');
        
        if (!Array.isArray(keys))
          keys = [keys];
  
        for (let key of keys) {
          let single = state.parse.single[key];
          if (single) {
            single.save();
            if (key == 'parish')
              state.parse.parishAccount.save();
            continue;
          }
  
          let multi = state.parse.multi[key];
          if (!multi)
            continue;
  
          for (let p_item of multi)
            p_item.destroy();
  
          multi = [];
          for (let item of state.parish[key]) {
            if (item['_example'])
              continue;
    
            let p_itemClass = Parse.Object.extend(firstCharCase(key, true));
            let p_item = new p_itemClass();
            p_item.set('account', state.parse.parishAccount);
    
            switch (key) {
              case 'staff':
                p_item.set('nameFirst', item.nameFirst);
                p_item.set('nameLast', item.nameLast);
                p_item.set('phone', item.phone);
                p_item.set('email', item.email);
        
                for (let p_position of state.parse.common.position) {
                  if (p_position.get('name') == item.position)
                    p_item.set('position', p_position);
                }
        
                break;
      
              case 'massTime':
                for (let p_type of state.parse.common.massType) {
                  if (p_type.get('name') == item.type)
                    p_item.set('massType', p_type);
                }
                for (let p_lang of state.parse.common.parishConnectLanguage) {
                  if (p_lang.get('encodedValue') == item.language)
                    p_item.set('language', p_lang);
                }
                p_item.set('timeStart', str2date(item.timeStart));
                p_item.set('timeEnd', str2date(item.timeEnd));
                array2days(item.days, p_item);
        
                break;
      
              case 'confessionTime':
                for (let p_lang of state.parse.common.parishConnectLanguage) {
                  if (p_lang.get('encodedValue') == item.language)
                    p_item.set('language', p_lang);
                }
                p_item.set('timeStart', str2date(item.timeStart));
                p_item.set('timeEnd', str2date(item.timeEnd));
                array2days(item.days, p_item);
        
                break;
      
              case 'otherTime':
                for (let p_type of state.parse.common.otherTimeType) {
                  if (p_type.get('name') == item.type)
                    p_item.set('type', p_type);
                }
                for (let p_lang of state.parse.common.parishConnectLanguage) {
                  if (p_lang.get('encodedValue') == item.language)
                    p_item.set('language', p_lang);
                }
                p_item.set('timeStart', str2date(item.timeStart));
                p_item.set('timeEnd', str2date(item.timeEnd));
                array2days(item.days, p_item);
        
                break;
      
              case 'sacrament':
              case 'ministry':
              case 'otherProgram':
                p_item.set('name', item.name);
                p_item.set('detail', item.detail);
        
                break;
  
              case 'sliderMedia':
                p_item.set('position', item.position);
                p_item.set('image', item.image);
    
                break;
            }
    
            p_item.save().then(null, e => console.log(e));
            multi.push(p_item);
          }
        }
      }
    },
  }
  
};
