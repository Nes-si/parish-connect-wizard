import Vue from 'vue';
import Vuex from 'vuex';

import nav from 'store/nav';
import user, {userPlugin} from 'store/user';
import data from 'store/data';
import initialize from 'store/initialize';


Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    nav,
    user,
    data,
    initialize
  },
  plugins: [userPlugin],
  //strict: process.env.NODE_ENV !== 'production'
});
