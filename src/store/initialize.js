import {Parse} from 'parse';

//const ENDPOINT = "https://parse.nuwe.co:49178/parse";
const ENDPOINT =
  process.env.NODE_ENV === "production"
  ? "http://parish.host/parse"
  : "http://localhost:1337/parse";

const APP_ID = "parish-connect";


function subInitParse() {
  Parse.initialize(APP_ID);
  Parse.serverURL = ENDPOINT;
}

export default {
  state: {
    loading: true,
    
    accountId: ''
  },

  actions: {
    initializeStart ({commit, dispatch}) {
      subInitParse();
      
      //set account id if exists
      let params = window.location.search;
      if (params) {
        if (params[0] == '?')
          params = params.slice(1);
        let array = params.split('&');
        array = array[0].split('=');
        if (array[0] == 'id' && array[1])
          commit('setAccountId', array[1]);
      }
      
      dispatch('getLocalStorage');
    }
  },

  mutations: {
    setAccountId (state, id) {
      state.accountId = id;
    },
    
    setLoadingState (state, value) {
      state.loading = value;
    }
  }
};
