import localizations from 'localizations.json';


export default {
  state: {
    section: 0,
    step: 1,
    maxSteps: Number,
  
    currentStepErrors: {},
    showErrors: false,
  
    completionFieldStatus: [[[]]],
  
    language: 'en',
    languageKeys: {}
  },
  
  mutations: {
    updateCompletionStatus (state, payload) {
      // check if current section/step index exists in array
      if (!state.completionFieldStatus[state.section - 1])
        state.completionFieldStatus[state.section - 1] = [];
      
      let curSect = state.completionFieldStatus[state.section - 1];
    
      if (!curSect[state.step - 1])
        curSect[state.step - 1] = [];
      
      let curStep = curSect[state.step - 1];
    
      let targetIndex = -1;
      for (let i = 0; i < curStep.length; i++) {
        if (curStep[i].key == payload.key) {
          targetIndex = i;
          break;
        }
      }
    
      if (targetIndex != -1) {
        if (payload.value)
          curStep[targetIndex].value = payload.value;
      } else {
        curStep.push(payload);
      }
    },
  
    applyLanguage (state, lang) {
      //TODO: API: get localizations
      state.language = lang;
      state.languageKeys = localizations[lang];
      state.step++;
    },
  
    updateErrors (state, payload) {
      if (payload)
        state.currentStepErrors[payload.key] = payload.error;
    },
  
    setShowingErrors (state, value) {
      state.showErrors = value;
    },
  
    incrementStep (state) {
      state.currentStepErrors = {};
      state.step++;
    },
  
    decrementStep (state) {
      state.showErrors = false;
      state.currentStepErrors = {};
      state.step--;
    },
  
    setSection (state, s) {
      state.currentStepErrors = {};
      state.section = s;
      state.step = 1;
    },
  
    finishSection(state) {
      state.showErrors = false;
      state.currentStepErrors = {};
  
      state.step = 1;
      state.section++;
    },
  
    setMaxSteps(state, mxs) {
      state.maxSteps = mxs;
    }
  }
  
};
