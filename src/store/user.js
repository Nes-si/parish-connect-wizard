import {Parse} from 'parse';

import {UserData} from 'models/UserData';


export const ERROR_USER_EXISTS  = 'app/user/ERROR_USER_EXISTS';
export const ERROR_WRONG_PASS   = 'app/user/ERROR_WRONG_PASS';
export const ERROR_WRONG_CODE   = 'app/user/ERROR_WRONG_CODE';

function checkCode(code) {
  return new Promise((resolve, reject) => {
    Parse.Cloud.run('checkRegCode', {code})
      .then(resolve, reject);
  });
}

export default {
  state: {
    localStorageReady: false,
    fetchingRequest: false,
  
    authorized: false,
    authError: null,
  
    email: '',
    password: '',
  
    userData: null
  },
  
  actions: {
    register({commit}, {email, password, code}) {
      commit('loginReq', {email, password});
  
      checkCode(code)
        .then(() => {
          let user = new Parse.User();
          user.set("username", email);
          user.set("email", email);
          user.set("password", password);
          user
            .signUp()
            .then(() => {
              Parse.User.logIn(email, password)
                .then(() => {
                  localStorage.setItem('authorization', JSON.stringify({email, password}));
                  let userData = new UserData().setOrigin();
                  commit('loginResp', {authorized: true, userData});
                }, () => {
                  commit('loginResp', {authError: 'FATAL ERROR!!! AAAAAA!!!'});
                });
            }, () => {
              commit('loginResp', {authError: ERROR_USER_EXISTS});
            });
        })
        .catch(() => {
          commit('loginResp', {authError: ERROR_WRONG_CODE});
        });
    },
  
    login({commit}, {email, password}) {
      commit('loginReq', {email, password});
    
      Parse.User.logIn(email, password)
        .then(() => {
          localStorage.setItem('authorization', JSON.stringify({email, password}));
          let userData = new UserData().setOrigin();
          commit('loginResp', {authorized: true, userData});
        }, () => {
          commit('loginResp', {authError: ERROR_WRONG_PASS});
        });
    },
    
    getLocalStorage({commit}) {
      let authStr = localStorage.getItem('authorization');
      if (authStr) {
        let auth = JSON.parse(authStr);
        commit('loginReq', {
          email: auth.email,
          password: auth.password
        });
      
        Parse.User.logIn(auth.email, auth.password)
          .then(() => {
            let userData = new UserData().setOrigin();
            commit('loginResp', {
              authorized: true,
              localStorageReady: true,
              userData
            });
          }, () => {
            commit('loginResp', {
              localStorageReady: true
            });
          });
      } else {
        commit('loginResp', {
          localStorageReady: true
        });
      }
    }
    
  },
  
  mutations: {
    loginReq (state, {email, password}) {
      state.fetchingRequest = true;
      state.authorized = false;
      state.authError = null;
      state.email = email;
      state.password = password;
    },
  
    loginResp (state, {userData, authorized, authError, localStorageReady}) {
      state.fetchingRequest = false;
      state.authorized = authorized;
      state.authError = authError;
      state.userData = userData;
      if (localStorageReady)
        state.localStorageReady = true;
    },
    
    logout (state) {
      localStorage.clear();
      Parse.User.logOut();
      
      state.authorized = false;
    }
  }
};


export const userPlugin = store => {
  store.subscribe((mutation, state) => {
    if (mutation.type == 'loginReq')
      store.commit('setLoadingState', true);
    
    if (mutation.type == 'loginResp') {
      if (mutation.payload.authorized)
        store.dispatch('initialData');
      else
        store.commit('setLoadingState', false);
    }
  })
};
